var userModel = {
    /*
    *  send user input to be saved
     *  @param: userData
     *  @param: handlers
    */
    createNew: function(userData, handlers) {
        $.ajax({
            url: "api/users/create.php",
            type: "POST",
            data: userData,
            success: function(data) {
                Cookies.remove('stepId');
                handlers.success(data)
            },
            error: handlers.error
        });
    },

    // save user input in field
    setUserInput : function(userInput, resultHandler){
        if(userInput)
            Cookies.set(userInput.field, userInput.value);
        resultHandler.success();
    } ,

    // get user old inputs
    getUserInput : function(resultHandler){
        var userInputs = Cookies.get();

        resultHandler.success(userInputs);
    },

    // save user last step
    setRegistrationStep: function(stepId){
        Cookies.set('stepId' , stepId)

        return true;
    },

};


var registerView = {
    installStepWizard: function(userInput , finishHandler , stepHandler , oldInputs) {
        var form = $("#register-form");
        form.validate({
            errorPlacement: function errorPlacement(error, element) {
                element.before(error);
            },
            rules: {
                confirm: {
                    equalTo: "#password"
                }
            }
        });
        form.children("div").steps({
            headerTag: "h3",
            bodyTag: "section",
            startIndex:parseInt(oldInputs.stepId ? oldInputs.stepId : 0),
            transitionEffect: "slideLeft",
            onStepChanging: function (event, currentIndex, newIndex) {
                form.validate().settings.ignore = ":disabled,:hidden";
                stepHandler(newIndex);
                return form.valid();
            },

            onFinished: function (event, currentIndex) {
                finishHandler(form.serialize());
            }
        });

        //$('#wizard').steps("setStep", 3);

        $('.form-control').focusout(function(){
            // save user input
            console.log({field:$(this).attr('name') , value:$(this).val()});
            userInput.call(this, {field:$(this).attr('name') , value:$(this).val()});
        });

        $.each(oldInputs , function (index,item) {
            if($('input[name=' + index + ']').length){
                $('input[name=' + index + ']').val(item);
            }
        })
    },

    successRegister: function(userPaymentId){
        $('#registerSteps').hide();
        $('#afterRegistration').show();
        $('#resultMsg').addClass('alert-success');
        $('#resultMsg').text("Successfully Registered your payment id is : " + userPaymentId);
    },

    failedRegister: function () {
        $('#registerSteps').hide();
        $('#afterRegistration').show();
        $('#resultMsg').addClass('alert-danger');
        $('#resultMsg').text("Couldn't create user");
    }
};

function Presenter(_view,_model) {
    var self = this;
    this.init = () => { // start from here (page load)
        _model.getUserInput({
            success:(oldInputs)=> {
                _view.installStepWizard(self.saveUserData , self.postUserInputs , self.saveUserStep , oldInputs);
            }
        });

    }

    // save user input to field
    this.saveUserData = (_userInput) => {
        _model.setUserInput(_userInput , {
            success: () => {
                console.log('saved')
            },
        });
    }

    // save user step
    this.saveUserStep = (stepId) => {
        _model.setRegistrationStep(stepId)
    };
    // send user data to be saved
    this.postUserInputs = (_userData) => {
        _model.createNew(_userData,{
            success :  (data) => {

                _view.successRegister(data.paymentDataId);
            },
            error: () => {
                _view.failedRegister();
            }
        })
    }


    self.init();

};

