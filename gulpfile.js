var gulp = require('gulp');

// Include plugins
var mainBowerFiles = require('main-bower-files');
var less = require('gulp-less');
var cssmin = require('gulp-cssmin');
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');
var rename = require('gulp-rename');
var order = require("gulp-order");
var browserSync = require('browser-sync').create();
var reload      = browserSync.reload;

gulp.task('scripts' , ['bower'], function() {
    return gulp.src('dist/lib/*.js')
        .pipe(order([
            'dist/lib/jquery.js',
            'dist/lib/bootstrap.js',
            'dist/lib/jquery.steps.js',
            'dist/lib/js.cookies.js'
        ] , {base:'./'}))
        .pipe(concat('main.js'))
        .pipe(uglify())
        .pipe(gulp.dest('build/js'));
});

gulp.task('app' , function() {
    return gulp.src('src/js/*.js')
        .pipe(order([
            'src/js/model.js',
            'src/js/view.js',
        ] , {base:'./'}))
        .pipe(concat('app.js'))
        //.pipe(uglify())
        .pipe(gulp.dest('build/js'));
});

gulp.task('styles' , ['bower'], function() {
    return gulp.src('dist/lib/*.css')
        .pipe(concat('main.css'))
        // Minify the file
        .pipe(cssmin().on('error', function(err) {
            console.log(err);
        }))
        .pipe(gulp.dest('build/css'));
});


gulp.task('bower', function() {
    // mainBowerFiles is used as a src for the task,

    return gulp.src(mainBowerFiles())
        .pipe(gulp.dest('dist/lib'))
});

gulp.task('watch', function() {
    // Watch .js files
    gulp.watch('src/js/*.js', ['app']);

});

gulp.task('default', ['bower' , 'scripts' , 'styles' , 'app' , 'watch']);