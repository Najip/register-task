var userModel = {
    /*
    *  send user input to be saved
     *  @param: userData
     *  @param: handlers
    */
    createNew: function(userData, handlers) {
        $.ajax({
            url: "api/users/create.php",
            type: "POST",
            data: userData,
            success: function(data) {
                Cookies.remove('stepId');
                handlers.success(data)
            },
            error: handlers.error
        });
    },

    // save user input in field
    setUserInput : function(userInput, resultHandler){
        if(userInput)
            Cookies.set(userInput.field, userInput.value);
        resultHandler.success();
    } ,

    // get user old inputs
    getUserInput : function(resultHandler){
        var userInputs = Cookies.get();

        resultHandler.success(userInputs);
    },

    // save user last step
    setRegistrationStep: function(stepId){
        Cookies.set('stepId' , stepId)

        return true;
    },

};

