var registerView = {
    installStepWizard: function(userInput , finishHandler , stepHandler , oldInputs) {
        var form = $("#register-form");
        form.validate({
            errorPlacement: function errorPlacement(error, element) {
                element.before(error);
            },
            rules: {
                confirm: {
                    equalTo: "#password"
                }
            }
        });
        form.children("div").steps({
            headerTag: "h3",
            bodyTag: "section",
            startIndex:parseInt(oldInputs.stepId ? oldInputs.stepId : 0),
            transitionEffect: "slideLeft",
            onStepChanging: function (event, currentIndex, newIndex) {
                form.validate().settings.ignore = ":disabled,:hidden";
                stepHandler(newIndex);
                return form.valid();
            },

            onFinished: function (event, currentIndex) {
                finishHandler(form.serialize());
            }
        });

        //$('#wizard').steps("setStep", 3);

        $('.form-control').focusout(function(){
            // save user input
            console.log({field:$(this).attr('name') , value:$(this).val()});
            userInput.call(this, {field:$(this).attr('name') , value:$(this).val()});
        });

        $.each(oldInputs , function (index,item) {
            if($('input[name=' + index + ']').length){
                $('input[name=' + index + ']').val(item);
            }
        })
    },

    successRegister: function(userPaymentId){
        $('#registerSteps').hide();
        $('#afterRegistration').show();
        $('#resultMsg').addClass('alert-success');
        $('#resultMsg').text("Successfully Registered your payment id is : " + userPaymentId);
    },

    failedRegister: function () {
        $('#registerSteps').hide();
        $('#afterRegistration').show();
        $('#resultMsg').addClass('alert-danger');
        $('#resultMsg').text("Couldn't create user");
    }
};
