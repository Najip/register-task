function Presenter(_view,_model) {
    var self = this;
    this.init = () => { // start from here (page load)
        _model.getUserInput({
            success:(oldInputs)=> {
                _view.installStepWizard(self.saveUserData , self.postUserInputs , self.saveUserStep , oldInputs);
            }
        });

    }

    // save user input to field
    this.saveUserData = (_userInput) => {
        _model.setUserInput(_userInput , {
            success: () => {
                console.log('saved')
            },
        });
    }

    // save user step
    this.saveUserStep = (stepId) => {
        _model.setRegistrationStep(stepId)
    };
    // send user data to be saved
    this.postUserInputs = (_userData) => {
        _model.createNew(_userData,{
            success :  (data) => {

                _view.successRegister(data.paymentDataId);
            },
            error: () => {
                _view.failedRegister();
            }
        })
    }


    self.init();

};

