<?php
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: access");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Allow-Credentials: true");
header('Content-Type: application/json');

// get database connection
include_once '../core/dbClient.php';

// instantiate user object
include_once '../core/User.php';

$database = new dbClient();
$db = $database->getConnection();
$user = new User($db);

// get posted data
$data = $_POST;

$user->first_name = $data['firstName'];
$user->last_name = $data['lastName'];
$user->phone = $data['phone'];
$user->street = $data['street'];
$user->house_number = $data['houseNumber'];
$user->zip_code = $data['zipCode'];
$user->city = $data['city'];
$user->account_owner = $data['accountOwner'];
$user->iban = $data['accountIban'];

// create the User
if($user->create()) {
    echo json_encode(['success' => true , 'paymentDataId' => $user->paymentDataId]);
}else{
    http_response_code(401);
    echo  json_encode(['success' => false , 'error' => "Couldn't create user"]);
}
