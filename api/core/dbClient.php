<?php
require_once("../config/dbConfig.php");
class dbClient{
    public $dbconn ;
    private $userName ;
    private $password;
    private $dbname;
    private $hostname;

    function __construct(){
        $this->userName = constant("username");
        $this->password = constant("password");
        $this->dbname = constant("dbname");
        $this->hostname = constant("hostname");
    }

    // get the database connection
    public function getConnection(){

        $this->dbconn = null;

        try{
            $this->dbconn = new PDO("mysql:host=" . $this->hostname . ";dbname=" . $this->dbname, $this->userName, $this->password);
            $this->dbconn->exec("set names utf8");
        }catch(PDOException $exception){
            echo "Connection error: " . $exception->getMessage();
        }

        return $this->dbconn;
    }

    public function openDB(){
        try{
            $this->dbconn = new mysqli($this->hostname, $this->userName, $this->password, $this->dbname)or die("Unable to connect to MySQL");
        }catch(Exception $e){
            return $e->getMessage();
        }

    }

    public function closeDB(){
        mysqli_close($this->dbconn);
    }
}
?>