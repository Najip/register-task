<?php
include_once 'util.php';
class User{

    use util;
    // database connection and table name
    private $conn;
    private $table_name = "users";

    // object properties
    public $user_id;
    public $first_name;
    public $last_name;
    public $phone;
    public $street;
    public $house_number;
    public $zip_code;
    public $city;
    public $account_owner;
    public $iban;
    public $paymentDataId;
    public $created_at;
    public $updated_at;

    // constructor with $db as database connection
    public function __construct($db){
        $this->conn = $db;
    }

    // create user
    function create(){

        // query to insert record
        $query = "INSERT INTO
                " . $this->table_name . "
            SET
                first_name=:first_name, last_name=:last_name, phone=:phone, street=:street, house_number=:house_number , zip_code=:zip_code , city=:city , created_at=:created_at , updated_at=:updated_at";

        // prepare query
        $stmt = $this->conn->prepare($query);

        // sanitize
        $this->first_name=htmlspecialchars(strip_tags($this->first_name));
        $this->last_name=htmlspecialchars(strip_tags($this->last_name));
        $this->phone=htmlspecialchars(strip_tags($this->phone));
        $this->street=htmlspecialchars(strip_tags($this->street));

        $this->house_number=intval($this->house_number);
        $this->zip_code=htmlspecialchars(strip_tags($this->zip_code));
        $this->city=htmlspecialchars(strip_tags($this->city));
        $this->account_owner=htmlspecialchars(strip_tags($this->account_owner));
        $this->iban=htmlspecialchars(strip_tags($this->iban));

        $this->created_at=date('Y-m-d H:i:s');
        $this->updated_at=date('Y-m-d H:i:s');

        // bind values
        $stmt->bindParam(":first_name", $this->first_name);
        $stmt->bindParam(":last_name", $this->last_name);
        $stmt->bindParam(":phone", $this->phone);
        $stmt->bindParam(":street", $this->street);
        $stmt->bindParam(":house_number", $this->house_number);

        $stmt->bindParam(":zip_code", $this->zip_code);
        $stmt->bindParam(":city", $this->city);
        $stmt->bindParam(":created_at", $this->created_at);
        $stmt->bindParam(":updated_at", $this->updated_at);

        // execute query

        if($stmt->execute()){
            // payment data
            $this->user_id = $this->conn->lastInsertId();
            $paymentData = [
                "customerId" => $this->user_id,
                "iban" => $this->iban,
                "owner" => $this->account_owner
            ];


            if($this->addPaymentInformation($paymentData) && $this->updateUserPaymentId()){
                return true;
            }
        }

        return false;

    }

    private function addPaymentInformation($paymentData){
        // send payment data
        $payment_response = json_decode($this->doPost("https://37f32cl571.execute-api.eu-central-1.amazonaws.com/default/wunderfleet-recruiting-backend-dev-save-payment-data",$paymentData),true);

        if($payment_response){
            $this->paymentDataId = $payment_response['paymentDataId'];

            return true;
        }

        return false;
    }

    private function updateUserPaymentId(){
        //query to insert record
        $query = "UPDATE 
                " . $this->table_name . "
            SET
                paymentDataId=:paymentDataId,updated_at=:updated_at WHERE id=:user_id";

        // prepare query
        $stmt = $this->conn->prepare($query);
        $this->updated_at = date('Y-m-d H:i:s');
        // bind values
        $stmt->bindParam(":paymentDataId", $this->paymentDataId);
        $stmt->bindParam(":updated_at",$this->updated_at);
        $stmt->bindParam(":user_id", $this->user_id);


        if($stmt->execute()){
            return true;
        }

        return false;
    }


}